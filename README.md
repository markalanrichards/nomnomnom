# nomnpm

I created this for fun after watching a friend mistype npm as nom.

Now you can nom nom your npm.

After installation, try running `nom`

## Quality
There is no care for anything, but the simplest success case. So it will quite likely fail appropriate error handling, overuse memory for large stdout, risk hanging the underlying npm command and not handle control characters properly (despite making use of them). Please don't depend on this project for anything serious.

## Maintenance
I have no intent to maintain this and thus have no dependencies in it. I may well merge pull requests provided they:

* are small and digestible, with multiple PRs used for a complex improvement
* have no runtime dependencies added and minimal devDependencies.





