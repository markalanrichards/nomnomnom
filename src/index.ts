import { execFile } from "child_process";
const { exec } = require("child_process");
const sleep = (ms: number) => new Promise(resolve => setTimeout(resolve, ms));
const refreshRate = 100
export const nom = () => {
    const processArgv = (process.argv.length < 3) ? ["--help"] : process.argv.slice(2)
    const subprocess = execFile("npm", processArgv)   
    function processNextLinePosition(lines: Array<string>): Promise<number> {
        const linesLength = lines.length
        if (linesLength > 0) {
            const firstLine = lines.shift()
            process.stdout.write(firstLine);
            process.stdout.write("\n");
            process.stdout.write('\u001b[s');
            process.stdout.write('\u001b[1A');
            const lineLength = firstLine.length
            return processNextCharPosition(0, lineLength, () => processNextLinePosition(lines))
        }
        else {
            return new Promise(resolve => resolve(0))
        }
    }
    function processNextCharPosition(charPosition: number, lineLength: number, processNextLinePosition: () => Promise<number>): Promise<number> {
        if (lineLength == 0) {
            process.stdout.write('\u001b[u');
            return processNextLinePosition()
        }
        else if (charPosition < lineLength) {
            return sleep(refreshRate)
                .then(slept => {
                    process.stdout.write("<");
                    return sleep(refreshRate)
                })
                .then(slept => {
                    process.stdout.write('\u001b[1D');
                    process.stdout.write("-");
                    return sleep(refreshRate)
                })
                .then(slept => {
                    process.stdout.write('\u001b[1D');
                    process.stdout.write(" ");
                    return sleep(refreshRate)
                })
                .then(resolve => processNextCharPosition(charPosition + 1, lineLength, processNextLinePosition))
        }
        else {
            return processNextLinePosition()
        }
    }
    subprocess.stdout.on('data', data => {
        const lines = data.split("\n")
        processNextLinePosition(lines)

    })
}